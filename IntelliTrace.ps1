#启用PowerShell脚本执行：set-executionpolicy remotesigned
echo "----------------------------------------------------------------------------------------------"
echo "启用PowerShell脚本执行：set-executionpolicy remotesigned"
echo "----------------------------------------------------------------------------------------------"
echo ""
echo ""
echo ""
echo ""

# 脚本所在目录
$OriginDir="D:\IntelliTraceCollector"
# 管理员用户名
$Admin="administrator"
# 要调试的应用程序池
$AppPool="jhbapp"
$AppPoolUser="IIS APPPOOL\$AppPool"

# D:\IntelliTraceCollector、%cd%

# ----------------------------------------------------------------------------------------------

# 使用expand 命令来扩展 IntelliTraceCollection.cab，加上最后的句点（“.”）
echo "------------- 解压IntelliTraceCollection.cab -------------"
$executePath="$OriginDir\IntelliTraceCollection"
$isExecutePathExists = Test-Path $executePath
if (-not $isExecutePathExists)
{
    mkdir $executePath
}

expand /f:* $OriginDir\IntelliTraceCollection.cab $executePath

cd $executePath

echo ""
echo "------------- 使用Windows icacls 命令授予服务器管理员访问收集器目录的完全权限 -------------"
$right=$Admin+':F'
icacls $executePath /grant $right

echo ""
echo "------------- 授予 Web 应用或 SharePoint 应用程序的应用程序池对收集器目录的读取和执行权限。 -------------"
$right=$AppPoolUser+':RX'
icacls $executePath /grant $right

# ----------------------------------------------------------------------------------------------
echo ""
echo "------------- 安装 IntelliTrace PowerShell cmdlet 收集 Web 应用程序或 SharePoint 应用程序的数据 -------------"

Import-Module $executePath\Microsoft.VisualStudio.IntelliTrace.PowerShell.dll

# ----------------------------------------------------------------------------------------------
echo ""
echo "------------- 设置 .iTrace 文件目录的权限 -------------"
$logDir="$executePath/../IntelliTraceLog"
$isLogDirExists = Test-Path $logDir

if (-not $isLogDirExists)
{
    cd $executePath/../
    mkdir IntelliTraceLog

    cd IntelliTraceLog    
}

echo ""
echo "针对 Web 应用或 SharePoint 应用程序，授予其应用程序池对 .iTrace 文件目录的完全权限。"
$right=$AppPoolUser+':F'
icacls $logDir /grant $right

# ----------------------------------------------------------------------------------------------
echo ""
echo "------------- 从 Web 应用程序或 SharePoint 应用程序中收集数据 -------------"

Start-IntelliTraceCollection $AppPool $executePath\collection_plan.ASP.NET.default.xml $executePath/../IntelliTraceLog


cd $OriginDir
# 要获得 .iTrace 文件的快照，请使用该语法：
# Get-IntelliTraceCollectionStatus

# 要停止收集数据，请使用该语法：
# Stop-IntelliTraceCollection “ <ApplicationPool> ”